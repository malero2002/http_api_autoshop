class Auto:
    def __init__(self, mark=None, car_id=None, size=None, name=None, engine=None, transmission=None,
                 engine_power=None, color=None, equipment=None, drive=None, total = None):
        self.mark = mark  # марка автомобиля
        self.car_id = car_id # id
        self.size = size  # класс автомобиля
        self.name = name  # название авто
        self.engine = engine  # тип двигателя (бензин, дизель)
        self.drive = drive  # привод
        self.transmission = transmission  # коробка передач
        self.engine_power = engine_power  # мощность двигателя
        self.color = color  # цвет авто
        self.equipment = equipment  # комплектация
        self.total = total
