import json

import requests
from flask import Flask, Response, request, json
from model.model_auto import Auto
from libs.utils import search_car
from libs.logger import log

logger = log()

app = Flask(__name__)
cars_list = []
colors_list = ['Black', 'Blue', 'Read', 'White', 'Green']


@app.route("/create_auto", methods=['POST'])
def create_auto():
    car = Auto()
    car.car_id = id(car)
    cars_list.append(car)
    logger.info(f'Сервер создал автомобиль c id = {car.car_id}')
    #print(f'Сервер создал автомобиль c id = {car.car_id}')
    return Response(status=201, headers={'Car-id': car.car_id})


@app.route("/set/color", methods=['POST'])
def set_color_auto():

    car_id = request.headers.get('Car-id')

    try:
        color = request.json["color"]
    except KeyError:
        logger.info('Вы допустили ошибку в "key"')
        return Response(status=400)

    car = search_car(car_id, cars_list)
    logger.info(f'Выберите цвет из {colors_list}')

    if car is not None:
        if (type(color) is str) and (color in colors_list):
            car.color = color
            logger.info(f'Автомобиль с id = {car.car_id} имеет цвет {car.color}')
            return Response(status=200)
        else:
            logger.info(f'Выберите цвет из предлагаемых вариантов!')
            return Response(status=400)
    else:
        logger.info(f'Автомобиля с id = {car_id} нет в списке созданных!')
        return Response(status=404)


@app.route("/set/mark", methods=['POST'])
def set_mark_auto():
    car_id = request.headers.get('Car-id')
    car = search_car(car_id, cars_list)

    if car is not None:
        car.mark = request.data
        logger.info(f'Автомобиль с id = {car.car_id} имеет марку {car.mark}')
        return Response(status=200)
    else:
        logger.info(f'Автомобиля с id = {car_id} нет в списке созданных!')
        return Response(status=404)


@app.route("/set/drive", methods=['POST'])
def set_drive_auto():
    car_id = request.headers.get('Car-id')
    car = search_car(car_id, cars_list)

    if car is not None:
        car.drive = request.json["drive"]
        logger.info(f"Автомобиль с id = {car.car_id} имеет {car.drive} привод")
        return Response(status=200)
    else:
        logger.info(f'Автомобиля с id = {car_id} нет в списке созданных!')
        return Response(status=404)


@app.route("/set/engine_power", methods=['POST'])
def set_engine_power_auto():
    car_id = request.headers.get('Car-id')
    car = search_car(car_id, cars_list)

    if car is not None:
        car.engine_power = request.json["power"]
        logger.info(f'Автомобиль с id = {car.car_id} имеет {car.engine_power} л.с.')
        return Response(status=200)
    else:
        logger.info(f'Автомобиля с id = {car_id} нет в списке созданных!')
        return Response(status=404)


@app.route("/info", methods=['POST'])
def info():
    car_id = int(request.headers.get('Car-id'))
    car = search_car(car_id, cars_list)

    if car is not None:
        total = {'id': str(car.car_id), 'color': str(car.color), 'mark': str(car.mark), 'drive': str(car.drive),
                     'power': str(car.engine_power)}
        return Response(response=json.dumps(total), status=200, mimetype='application/json')
    else:
        return {
            'reason': 'Данного автомобиля нет в списке созданных'
        }, 404

@app.route('/delete', methods=['POST'])
def delete():
    car_id = int(request.headers.get('Car-id'))
    car = search_car(car_id, cars_list)

    if car is not None:
        cars_list.remove(car)
        return Response(status=200)
    else:
        return {
            'reason': 'Данного автомобиля нет в списке созданных'
        }, 404


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
